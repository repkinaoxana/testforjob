<?php
header('Content-Type: text/html; charset=utf-8');

session_start();

include_once("functions.php");
include_once("db.php");
include_once("routing.php");

function load_page()
{
	$page=get_page();
	routing_page($page);	
}


function get_page()
{
	$result='home';
	
	$array=explode("/",$_SERVER['REQUEST_URI']);
	$page=$array[1];	
	$page=check_parameter($page);
	
	if ($page)
	{
		$result=$page;
	}
	
	return $result;	
}

function check_parameter($page)
{
	$result=$page;
	$array=explode("?",$page);

	if ($array[1])
	{
		$result=$array[0];
	}

	return $result;
}

?>