<?php

function routing_page($page)
{
	switch($page)
	{
		case 'home':
			$content="controller_home.php";
			break;

		case 'films':
			$content="controller_films.php";
			break;

		case 'add_film': // cтраница где добавляем
			$content="controller_add_film.php";
			break;	
	}

	if (!strstr($page, 'action'))
	{
		include_once("views/template_view.php");
	}
	else
	{
		include 'controllers/'.$content;
	}
}

?>