// форма для добавления новых данных
<div class="container-fluid col-xs-offset-1 col-sm-offset-2 col-md-offset-2 col-lg-offset-3 col-lg-6 col-md-8 col-sm-8 col-xs-10">
	<form method="post">
		<div class="form-group">
			<label>Title</label>
			<input type="text" name="name" class="form-control" placeholder="">
		</div>
		<div class="form-group">
			<label>Year</label>
			<input type="text" name="year" class="form-control" placeholder="">
		</div>
		<div class="form-group">
			<label>isActive</label>
			<p style="color: #B03060; font-size: 13px">* "1" for active, otherwise "0"</p>
			<input type="text" name="isActive" class="form-control" placeholder="1" value="<?echo 1; ?>">
		</div>
		<button type="submit" class="btn">Add new movie</button>
	</form>
</div>