﻿// страница, на которой отображаются все фильмы с пометкой true
<div class="container-fluid col-xs-offset-1 col-sm-offset-2 col-md-offset-2 col-lg-offset-3 col-lg-6 col-md-8 col-sm-8 col-xs-10">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Film</th>
				<th>Year</th>
			</tr>
		</thead>
		<tbody>
			<?php
			if ($films)
			{
				foreach($films as $film)
				{
					extract($film);
					?>  
					<tr>
						<td><?=$name?></td>
						<td><?=$year?></td>
						<?php
					}
				}
				?> 
			</tr> 
		</tbody>
	</table>
</div>